<?php

// Opprett ny sesjon
session_start();

// Kobling mot databasen
require_once 'includes/db.php';

// Sett standard-variabler
$uknownUser = false;
$failRegMsg = "The username has already been taken";
$unameSet = "";

/**
 * Hvis "$_POST['uname]" er satt forsøker brukeren å registrere seg. Valideringen som skjer
 * på klient siden er da allerede passert. Brukernavn og passord (hashet) legges i databas-tabellen
 * "users". Det foretas en sjekk som sjekker om brukernavnet allerede er tatt. Hvis det allerede
 * er i bruk, settes $uknownUser til true og brukernavnet blir "lagret". Hvis brukernavnet ikke er
 * tatt setter vi en ny sesjon.
 */
if (isset($_POST['uname'])) {
  $sql = 'INSERT INTO users (uname, password)
          VALUES (?, ?)';
  $sth = $db->prepare($sql);
  $sth->execute(array($_POST['uname'], password_hash($_POST['password'], PASSWORD_DEFAULT)));
  if ($sth->rowCount()==0) {
    $uknownUser = true;
    $unameSet = 'value="'.$_POST['uname'].'"';
  } else {
    $id = $db->lastInsertId();
    $_SESSION['id'] = $id;
    header('Location: oppgave2.php'); // Sender brukeren til oppgave2.php etter fullført registrering
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Oppgave 3</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
  <link rel="stylesheet" href="style.css" />
</head>
<body>
  <!-- Dette er eksempelkode fra labøvingen "bookmark-persistent-login", filen "user.loginform.php" av Øyvind Kolloen -->
  <div class="container">
    <!-- Sjekk om sesjonen er satt -->
   <?php if (isset($_SESSION['id'])) { ?>
    Logg ut <a href="oppgave2.php?logout=true">her</a>
    <h2>Last opp bilde <a href="oppgave4.php">her</a></h2>
   <?php } else { ?>
    <div style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info">
        <div class="panel-heading">
          <div class="panel-title">Registrer deg</div>
        </div>
        <div class="panel-body">
          <!-- Denne meldingen vil vises om brukeren allerede eksisterer -->
          <?php if ($uknownUser) {
              echo '<div class="alert alert-danger" role="alert">'.$failRegMsg.'</div>';
            } ?>
          <form class="form-horizontal" role="form" method="post">
            
            <!-- Brukernavn -->
            <div class="form-group">
              <label for="uname" class="col-md-3 control-label">Brukernavn</label>
              <div class="col-md-9">
                <input type="username" <?php echo $unameSet; ?> class="form-control" id="uname" name="uname" placeholder="Brukernavn">
                <span id="unameMsg" class="errorMsg"></span>
              </div>
            </div>
            
            <!-- Passord -->
            <div class="form-group">
              <label for="password" class="col-md-3 control-label">Passord</label>
              <div class="col-md-9">
                <input type="password" class="form-control" id="pwdOne" name="password" placeholder="Passord">
                <span id="pwdMsgOne" class="errorMsg"></span>
              </div>
            </div>
            
            <!-- Bekreft passord -->
            <div class="form-group">
              <label for="passwordVerify" class="col-md-4 control-label">Bekreft passord</label>
              <div class="col-md-8">
                <input type="password" class="form-control" id="pwdTwo" name="password" placeholder="Bekreft passord">
                <span id="pwdMsgTwo" class="errorMsg"></span>
              </div>
            </div>
            
            <!-- Send skjema -->
            <div class="form-group">
              <div class="col-md-offset-3 col-md-9">
                <input type="submit" id="btn-signup" class="btn btn-info" value="Sign Up"/>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  <!-- Her slutter eksempelkoden -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    
    /**
     * Funksjonen sjekker om brukernavnet er tomt, og at det inneholder
     * minst 6 karakterer.
     *
     * @return boolean true/false ut fra om brukernavnet ikke er tomt 
     * og lengre enn 6 karakterer
     */
    function validateUname () {
      // Hent ut verdien brukeren har skrevet inn
      var name = $("#uname").val();
      
      // Sjekk om navnet inneholder noen karakterer
      if (name !== "") {
        // Sjekk om navnet inneholder minst seks karakterer
        if (name.length >= 6) {
          $("#unameMsg").hide();
          $("#unameMsg").prev().removeClass("errorMsg");
          return true;
        } else {
          $("#unameMsg").html("Brukernavnet på inneholde minst seks karakterer").show();
          $("#unameMsg").prev().addClass("errorMsg");
          return false;
        }
      } else {
        $("#unameMsg").html("Dette feltet kan ikke være tomt").show();
        $("#unameMsg").prev().addClass("errorMsg");
        return false;
      }
    }
    
    /**
     * Funksjonen sjekker om passorder er tomt, og at det inneholder
     * minst 8 karakterer.
     *
     * @return boolean true/false ut fra om passordet ikke er tomt
     * og lenger enn 8 karakterer
     */
    function validatePasswordLength () {
      var pwdOne = $('#pwdOne').val();
      
      // Sjekk om navnet inneholder noen karakterer
      if (pwdOne !== "") {
        // Sjekk om navnet inneholder minst åtte karakterer
        if (pwdOne.length >= 8) {
          $("#pwdMsgOne").hide();
          $("#pwdMsgOne").prev().removeClass("errorMsg");
          return true;
        } else {
          $("#pwdMsgOne").html("Passordet må være minst åtte karakterer").show();
          $("#pwdMsgOne").prev().addClass("errorMsg");
          return false;
        }
      } else {
        $("#pwdMsgOne").html("Dette feltet kan ikke være tomt").show();
        $("#pwdMsgOne").prev().addClass("errorMsg");
        return false;
      }
    }
    
    /**
     * Funksjon som sjekker om passordene er like.
     *
     * @return true/false ut fra om passordene er like eller ikke.
     */
    function validatePasswordMatch () {
      var passwordOne = $('#pwdOne').val();
      var passwordTwo = $('#pwdTwo').val();
      
      if (passwordOne !== passwordTwo) {
        $("#pwdMsgTwo").html("Passordene matcher ikke").show();
        $("#pwdMsgTwo").prev().addClass("errorMsg");
        return false;
      } else {
        $("#pwdMsgTwo").hide();
        $("#pwdMsgTwo").prev().removeClass("errorMsg");
        return true;
      }
    }
    
    /**
     * Funksonen vil returnerene en boolean true/false ut fra om alle valideringene
     * er stemmer eller ikke.
     */
    function validateForm () {
      return (validateUname() && validatePasswordLength() && validatePasswordMatch());
    }
    
    /**
     * Funksjonen initaliserer når brukeren sender skjemaet. Det inneholder er if-statement
     * som sjekker om form-dataen er valid.
     *
     * @return boolean true/false ut fra om skjemaet er valid eller ikke.
     */
    $('form').on('submit', function(e) {
      if (!validateForm()) {
        return false;
      }
      return true;
    });
    
  </script>
</body>
</html>
