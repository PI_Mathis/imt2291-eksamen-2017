<?php

// Kobling mot databasen
require_once 'includes/db.php';

//Denne spørringen vil hente ut alle studieprogram med oppstart i 2016.
$sql ='SELECT DISTINCT studyProgramContent.subject, studyProgramContent.type,
                       studyProgramContent.semester, subject.name, subject.credits
        FROM studyProgramContent
        LEFT JOIN subject ON studyProgramContent.subject = subject.code
        WHERE studyProgramContent.startYear = 2016
        ORDER BY studyProgramContent.semester, subject.credits';
$sth = $db->prepare($sql);
$sth->execute(array());
$rows = $sth->fetchAll(PDO::FETCH_ASSOC); // assosiativ array som inneholder responsen fra spørringen

/**
 * Denne spørringen vil kun hente ut nanvet på studieprogrammet
 */
$sql1 = "SELECT name FROM studyprogram";
$sth1 = $db->prepare($sql1);
$sth1->execute(array());
$singleRow = $sth1->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Oppgave 8</title>
  <style>
      th.semester {
        width: 50px;
        text-align: right;
      }

      td.name {
        width: 400px;
      }

      td.type {
        width: 40px;
        text-align: center;
      }

      td.code {
        width: 100px;
      }

      table {
        border-spacing: 0;
        border-collapse: collapse;
      }

      tbody td {
        border-bottom: 1px solid grey;
      }
    </style>
</head>
<body>
  <!-- Skriver ut navnet på studieprogrammet -->
  <h1><?php echo $singleRow['name']; ?></h1>
  <table>
    <thead>
      <tr><th>Emnekode</th><th>Emnenavn</th><th>O/V</th><th colspan='6'>Studiepoeng pr semester</th></tr>
        <tr><th colspan='3'></th><th class='semester'>S1(h)</th><th class='semester'>S2(v)</th><th class='semester'>S3(h)</th><th class='semester'>S4(v)</th><th class='semester'>S5(h)</th><th class='semester'>S6(v)</th></tr>
    </thead>
    <tbody>
     <!-- Vi looper over responsen og skriver den ut i tabellen -->
     <!-- Vi sjekker også om semesteret stemmer i korte if-setninger -->
      <?php foreach ($rows as $row) { ?>
        <tr>
          <td><?php echo $row['subject']; ?></td>
          <td><?php echo $row['name']; ?></td>
          <td><?php echo strtoupper(substr($row['type'], 0, 1)); ?></td>
          <td><?php echo ($row['semester'] == 1) ? $row['credits'] : ""; ?></td>
          <td><?php echo ($row['semester'] == 2) ? $row['credits'] : ""; ?></td>
          <td><?php echo ($row['semester'] == 3) ? $row['credits'] : ""; ?></td>
          <td><?php echo ($row['semester'] == 4) ? $row['credits'] : ""; ?></td>
          <td><?php echo ($row['semester'] == 5) ? $row['credits'] : ""; ?></td>
          <td><?php echo ($row['semester'] == 6) ? $row['credits'] : ""; ?></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</body>
</html>