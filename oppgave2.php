<?php

// Start en ny sesjon
session_start();

// Kobling mot databasen
require_once 'includes/db.php';

// Sett standard-verdier
$failedLogin = false;
$failMsg = "Wrong username or password";
$unameSet = 'value=""';
$uname = "";

/**
 * Hvis "$_GET['logout]" er satt, betyr det at brukeren forsøker å logge seg ut.
 * Derfor bruker vi unset til å ikke sette sesjonsvariabelen lenger, og redirecter
 * brukeren tilbake til innloggingssiden.
 *
 * Hvis "$_POST['uname']" er satt forsøker brukeren å logge seg på. Vi henter ut id,
 * brukernavn og passord fra databasen, og sjekker mot brukernavnet som er skrevet inn.
 * Vi verifiserer også at passordet stemmer overens med det hashede passordet som ligger
 * i databasen. Hvis påloggingsopplysningene til brukeren stemmer, setter vi en ny 
 * sesjonsvariabel og lagrer brukernavnet. Hvis påloggingen misslykkes setter vi en 
 * boolean til false og "lagrer" brukernavnet som ble skrevet inn, slik at brukeren 
 * slipper å skrive det på nytt.
 *
 * Inne i HTML-koden sjekker vi om en sesjon med brukerid er satt. Om den er det, viser vi
 * beskyttet innhold. Om den ikke er satt viser vi skjemaet for pålogging.
 */
if (isset($_GET['logout'])) {
  unset($_SESSION['id']);
  header('Location: oppgave2.php');
} else if (isset($_POST['uname'])) {
  $sql = "SELECT id, uname, password FROM users WHERE uname = ?";
  $sth = $db->prepare($sql);
  $sth->execute(array($_POST['uname']));
  $row = $sth->fetch(PDO::FETCH_ASSOC);
  // Sjekk om passordet matcher
  if (isset($_POST['password'])&&password_verify($_POST['password'], $row['password'])) {
    $_SESSION['id'] = $row['id'];
    $_SESSION['uname'] = $row['uname'];
  } else {
    $failedLogin = true;
    $unameSet = 'value="'.$_POST['uname'].'"';
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Login Form</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
  <link rel="stylesheet" href="signin.css"/>
</head>
  <body>
    <div class="container">
      <!-- Vi sjekker om sesjonen er satt -->
      <?php if(isset($_SESSION['id'])) { ?>
        <h4>Hei, <?php echo $_SESSION['uname']; ?>! Logg ut ved å klikke <a href="oppgave2.php?logout=true">her</a></h4>
        <h4>Last opp bilde <a href="oppgave4.php">her</a></h4>
      <?php } else { ?>
      <!-- Denne er eksempelkode for innlogging med noen modifikasjon som brukes i labøvingen "login" av Øyvind Kolloen -->
      <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Logg Inn</h2>
        <div class="alert alert-info">
        Brukernavn: 140697<br />
        Passord: test
        </div>
        <?php
        if ($failedLogin) {
          echo '<div class="alert alert-danger" role="alert">'.$failMsg.'</div>';
        }
        ?>
        <label for="inputUname">Brukernavn</label>
        <input type="uname" id="inputUname" class="form-control" name="uname" placeholder="Username" <?php echo $unameSet; ?> autofocus />
        <label for="inputPassword">Passord</label>
        <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" />
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
      <!-- Her slutter eksempelkoden -->
      <?php } ?>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
  </body>
</html>