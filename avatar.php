<?php

// Denne koden er hentet fra bookmarks_week3_solution, filen thubnail.php av Øyvind Kolloen

header ("Content-type: image/jpeg"); // Sender tilbake bilde i jpeg format
$maxHeight = 150; // ønsket høyde
$maxWidth = 150; // ønsket bredde

$content = file_get_contents($_GET['url']);	// Hent ut innholdet i filem
$img = imagecreatefromstring($content);		// Konverter strengen (binær data) til bilde
$width = imagesx($img); // original bredde
$height = imagesy($img); // original høyde

// Skaler til riktig størrelse
$scaleX = $width/$maxWidth;
$scaleY = $height/$maxHeight;

$scale = ($scaleX>$scaleY)?$scaleX:$scaleY; // Skalere mest i høyde eller bredde
$scale = ($scale>1)?$scale:1; // Bruker veldig lite bilde, istedenfor å skalere det opp

// Skaler bilde
$scaled = imagecreatetruecolor($width/$scale, $height/$scale);
imagecopyresampled($scaled, $img, 0, 0, 0, 0, $width/$scale, $height/$scale, $width, $height);

// Returner det skalerte bilde i nettleseren
imagejpeg ($scaled, null, 100);