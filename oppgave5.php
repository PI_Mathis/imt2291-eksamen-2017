<?php

// Start en ny sesjon
session_start();

// Kobling mot databasen
require_once 'includes/db.php';

// Vi henter ut id, brukernavn og størrelsen på avatarbilde fra brukertabellen
$sql = 'SELECT id, uname, OCTET_LENGTH(avatar) AS avatarSize 
        FROM users';

$sth = $db->prepare($sql);
$sth->execute(array());
// Assosiativ array med responsen fra spørringen vi gjorde
$userData = $sth->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Last opp avatar</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
</head>
<body>
  <div class="container">
    <!-- Denne ker eksempelkode fra labøvingen "bookmarks_week3_solution", filen "bookmarkListing.inc.php" av Øyvind Kolloen -->
    <?php if (isset($_SESSION['id'])) { 
    // Loop over alle brukerne
    foreach ($userData as $user) { ?>
       <div class="row">
         <div class="col-xs-2">
        <!-- Sjekk om avatarstørrelsen er større enn 0 (ingen avatar-bilde) -->
        <?php if ($user['avatarSize'] > 0) {
            echo "<img class='img-responsive' src='avatarer.php?id={$user['id']}' />";  
        } ?>
         </div>
      <div class="col-xs-9">
        <h3><?php echo "".$user['uname'].""; ?></h3>
      </div>
    </div>
    <?php } 
        } else {
          header('Location: oppgave2.php');
        } ?>
    <!-- Her slutter det -->
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>