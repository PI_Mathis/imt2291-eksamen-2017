<?php

// Start ny sesjon
session_start();

// Kobling mot databasen
require_once 'includes/db.php';

/**
 * Hvis "$_POST['submit']" er satt forsøker brukeren å laste opp et avatar-bilde. Siden vi ønsker å skalere
 * bilde til størrelsen 150x150 må vi først kjøre et skript på bilde som gjennomfører dette, før bildet lagres
 * i databasen. Vi sender bilde over url som $_GET['url']. "avatar.php"-skriptet mottar bildet, og skalererer det.
 * Videre leser vi inn det skalerte bilde, og lagrer det i variabelen $avatar. Denne informasjonen lagres så i db.
 */
if (isset($_POST['submit'])) {
  
  // Opprett en url for å hente ut avatar-bilde fra avatar.php skriptet
  $host = 'http://'.$_SERVER['SERVER_ADDR'].':'.$_SERVER['SERVER_PORT'].$_SERVER['PHP_SELF'];
  $host = substr($host, 0, strrpos($host, "/"));
  $avatarurl =  $host.'/avatar.php?url='.urlencode($_FILES['avatar']['tmp_name']);
  $avatar = file_get_contents($avatarurl);	// Hent ut avatarbilde
  
  // Oppdater avataren til brukeren i databasen
  $sql = "UPDATE users 
          SET avatar = ? 
          WHERE id = ?";
  $sth = $db->prepare($sql);
  $sth->execute(array($avatar, $_SESSION['id']));
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Last opp avatar</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
</head>
<body>
  <div class="container">
    <?php if (isset($_SESSION['id'])) { ?>
      <div style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
          <div class="panel-heading">
            <div class="panel-title">Last opp avatar</div>
          </div>
          <div class="panel-body">
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
            
              <!-- Last opp avatar -->
              <div class="form-group">
                <label for="uploadAvatar" class="col-md-4 control-label">Last opp avatar:</label>
                <input type="file" name="avatar" />
              </div>
              
              <!-- Submit knapp -->
              <div class="form-group">
                <div class="col-md-offset-3 col-md-9">
                  <input class="btn btn-success" name="submit" type="submit" value="Lagre avatar"/>
                </div>
              </div>
            </form>
          </div>
        </div>
    </div>
    <?php } else { 
      header('Location: oppgave2.php');
     } ?>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>