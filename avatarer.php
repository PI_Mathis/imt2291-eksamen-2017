<?php
/**
 * Denne koden er tatt fra bookmarks_week3_solution, fil bookmarkListing.inc.php av Øyvind Kolloen
 *
 * Dette skriptet brukes for å vise avatar-bilder sammen med brukernavn i filen oppgave5.php.
 * Den kalles sammen med id'en til brukeren som parameter.
 */
header ("Content-type: image/jpg");	// Alle avatar bilder er lagret som jpg
require_once 'includes/db.php';		// Kobling mot databasen

$sql = 'SELECT avatar FROM users WHERE id=?';
$sth = $db->prepare($sql);
$sth->execute (array ($_GET['id']));	// Hent bilde fra databasem
if ($row = $sth->fetch())				// Vi fant bilde
	echo $row['avatar'];				// Returner bilde
