<?php

/**
 * Dette er kode fra prosjekt nummer en, ntnutube. De eneste modifikasjonene som er er gjort
 * innebærer endring av databasenavn og sql-fil.
 */

class Database {
  private $pdo;
  
  /**
   * @param - denne konstruktøren aksepterer ingen argumenter.
   *
   * Det vil først opprettes en PDO-instanse som representerer koblingen til
   * databasen. Videre gjennomføres en spørring mot "information_schema.schemata".
   * Vi får en boolean-verdi ut fra om databasen allerede eksisterer eller ikke.
   * Hvis den allerede eksisterer, bruker vi den. Hvis den ikke eksisterer, oppretter
   * vi databasen og leser inn innholdet fra sql-filen "studyPrograms.sql". Dette er en
   * fil som allerede lå i prosjektet. Den eneste endringen som er gjort i denne filen er
   * at vi la til tabellen "users".
   *
   * @return - denne konstruktøren returnerer ingenting
   */
  public function __construct() {
    
    $this->pdo = new PDO ('mysql:host=127.0.0.1', 'root', '');
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    
    $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'IMT2291Eksamen140697'";
    
    $dbExists = (bool) $this->pdo->query($query)->fetchColumn();
    
    if (!$dbExists) {

      $this->pdo->query('CREATE DATABASE IMT2291Eksamen140697');
      $this->pdo->query('USE IMT2291Eksamen140697');
      
      $db_sql = file_get_contents(__DIR__ . '/sql/studyPrograms.sql');
      $this->pdo->exec($db_sql);
    } else {
      $this->pdo->query('USE IMT2291Eksamen140697');
    }
  }
}

// Ved å opprette en ny instanse av Database-klassen vil koden i konstruktøren initialiseres
$my_db = new Database();

// Databasen er suksessfullt opprettet
echo "Database succesfully created!";
?>