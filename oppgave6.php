<?php

// Kobling mot databasen
require_once 'includes/db.php';

/**
 * Denne koden er hentet fra nettstedet https://www.joshmorony.com/part-1-using-the-http-service-in-ionic-to-dynamically-load-google-map-markers/
 */

// Hent ut id, brukernavn og filstørrelse på avatar
$sql = 'SELECT id, uname, OCTET_LENGTH(avatar) AS avatarSize
        FROM users';

$sth = $db->prepare($sql);
$res = $sth->execute(array());

$allUsers = array();

/**
 * @param størrelsen på bilde
 *
 * Funksjonen sjekker om størrelsen på bilde er større enn 0.
 * 
 * @return "y" eller "n" ut fra om brukeren har avatarbilde eller ikke.
 */
function checkAvatar ($size) {
  if ($size > 0) {
    return "y";
  } else {
    return "n";
  }
}

/**
 * Vi looper over alle brukerne i databasen, og nester de inn i en array med
 * assositative navn.
 */
while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
  
  $allUsers[] = array(
    'id' => $row['id'],
    'uname' => $row['uname'],
    'avatar' => checkAvatar($row['avatarSize'])
  );
}

// Vi setter disse headerne for å unngå CORS-problemer
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,x-prototype-version,x-requested-with');

/**
 * Vi sjekker om spørringen ble gjennomført som den skulle, og returnerer json-responsen
 * ut fra det. Hvis ikke returnerer vi success: false
 */
if ($res) {
  echo json_encode($allUsers); 
} else {
  echo "{'success':false}";
}

?>